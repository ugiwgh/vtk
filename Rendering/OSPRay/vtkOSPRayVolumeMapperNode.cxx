/*=========================================================================

   Program:   Visualization Toolkit
   Module:    vtkOSPRayVolumeMapperNode.cxx

   Copyright (c) Ken Martin, Will Schroeder, Bill Lorensen
   All rights reserved.
   See Copyright.txt or http://www.kitware.com/Copyright.htm for details.

      This software is distributed WITHOUT ANY WARRANTY; without even
      the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
      PURPOSE.  See the above copyright notice for more information.

 =========================================================================*/
#include "vtkOSPRayVolumeMapperNode.h"

#include "vtkAbstractVolumeMapper.h"
#include "vtkColorTransferFunction.h"
#include "vtkDataArray.h"
#include "vtkImageData.h"
#include "vtkObjectFactory.h"
#include "vtkOSPRayRendererNode.h"
#include "vtkPiecewiseFunction.h"
#include "vtkPointData.h"
#include "vtkVolume.h"
#include "vtkVolumeNode.h"
#include "vtkVolumeProperty.h"
#include "vtkInformation.h"
#include "vtkMatrix4x4.h"

#include "ospray/ospray.h"

#include <map>
#include <math.h>
#include <algorithm>


#include "ospray/ospray.h"

//============================================================================
vtkStandardNewMacro(vtkOSPRayVolumeMapperNode);

//----------------------------------------------------------------------------
vtkOSPRayVolumeMapperNode::vtkOSPRayVolumeMapperNode()
{
  this->NumColors = 128;
  this->SharedData = false;
  this->SamplingRate=0.0;
  this->TransferFunction = NULL;
}

// //----------------------------------------------------------------------------
vtkOSPRayVolumeMapperNode::~vtkOSPRayVolumeMapperNode()
{
  for (auto itr : Cache)
  {
    for (auto itr2 : itr.second)
    {
      ospRelease(itr2.second->VoxelData);
      ospRelease(itr2.second->Volume);
      delete itr2.second;
    }
  }
  if (this->TransferFunction)
    ospRelease(this->TransferFunction);
}

//----------------------------------------------------------------------------
void vtkOSPRayVolumeMapperNode::PrintSelf(ostream& os, vtkIndent indent)
{
  this->Superclass::PrintSelf(os, indent);
}

void vtkOSPRayVolumeMapperNode::Render(bool prepass)
{
  if (!TransferFunction)
  {
    TransferFunction = ospNewTransferFunction("piecewise_linear");
    ospCommit(TransferFunction);
  }
  if (prepass)
    {
    vtkVolumeNode* volNode = vtkVolumeNode::SafeDownCast(this->Parent);
    vtkVolume* vol = vtkVolume::SafeDownCast(volNode->GetRenderable());
    if (vol->GetVisibility() == false)
      {
      return;
      }
    vtkAbstractVolumeMapper* mapper = vtkAbstractVolumeMapper::SafeDownCast(this->GetRenderable());
    if (!vol->GetProperty())
      {
      vtkErrorMacro("VolumeMapper had no vtkProperty");
      return;
      }

    vtkOSPRayRendererNode *orn =
      static_cast<vtkOSPRayRendererNode *>(
        this->GetFirstAncestorOfType("vtkOSPRayRendererNode"));

    osp::Model* OSPRayModel = orn->GetOModel();
    if (!OSPRayModel)
    {
      return;
    }
    osp::Volume *OSPRayVolume = nullptr;


    // make sure that we have scalar input and update the scalar input
    if ( mapper->GetDataSetInput() == NULL )
    {
      vtkErrorMacro("VolumeMapper had no input!");
      return;
    }
    mapper->GetInputAlgorithm()->UpdateInformation();
    mapper->GetInputAlgorithm()->Update();

    vtkImageData *data = vtkImageData::SafeDownCast(mapper->GetDataSetInput());
    if (!data)
    {
      vtkErrorMacro("VolumeMapper's Input has no data!");
      return;
    }
    void* ScalarDataPointer =
      mapper->GetDataSetInput()->GetPointData()->GetScalars()->GetVoidPointer(0);
    int ScalarDataType =
      mapper->GetDataSetInput()->GetPointData()->GetScalars()->GetDataType();

    int dim[3];
    data->GetDimensions(dim);

    size_t typeSize = 0;
    std::string voxelType;
    if (ScalarDataType == VTK_FLOAT)
      {
      typeSize = sizeof(float);
      voxelType = "float";
      }
    else if (ScalarDataType == VTK_UNSIGNED_CHAR)
      {
      typeSize = sizeof(unsigned char);
      voxelType = "uchar";
      }
    else if (ScalarDataType == VTK_DOUBLE)
      {
      typeSize = sizeof(double);
      voxelType = "double";
      }
    else
      {
      std::cerr << "ERROR: Unsupported data type for ospray volumes, current supported data types are: "
       << " float, uchar, double\n";
       return;
      }
    //
    // Cache timesteps
    //
    double timestep=-1;
    vtkInformation *inputInfo = mapper->GetDataSetInput()->GetInformation();
    if (inputInfo && inputInfo->Has(vtkDataObject::DATA_TIME_STEP()))
      {
      timestep = inputInfo->Get(vtkDataObject::DATA_TIME_STEP());
      }

    vtkOSPRayVolumeCacheEntry* cacheEntry = Cache[vol][timestep];
    if (!cacheEntry)
      {
      cacheEntry = new vtkOSPRayVolumeCacheEntry();
      }
    OSPRayVolume = cacheEntry->Volume;
    //
    // create new ospray volume
    //
    if (cacheEntry->BuildTime < volNode->GetMTime())
      {
      if (cacheEntry->Volume)
        ospRelease(cacheEntry->Volume);
      if (SharedData)
        {
        cacheEntry->Volume = ospNewVolume("shared_structured_volume");
        }
      else
        {
        cacheEntry->Volume = ospNewVolume("block_bricked_volume");
        }
      OSPRayVolume = cacheEntry->Volume;

      //
      // Send Volumetric data to OSPRay
      //
      char* buffer = NULL;
      size_t sizeBytes = dim[0]*dim[1]*dim[2] *typeSize;

      buffer = (char*)ScalarDataPointer;
      ospSet3i(OSPRayVolume, "dimensions", dim[0], dim[1], dim[2]);
      double origin[3];
      double scale[3];
      vol->GetOrigin(origin);
      data->GetOrigin(origin);
      vol->GetScale(scale);
      double *bds = vol->GetBounds();
      origin[0] = bds[0];
      origin[1] = bds[2];
      origin[2] = bds[4];

      double spacing[3];
      data->GetSpacing(spacing);
      scale[0] = (bds[1]-bds[0])/double(dim[0]-1);
      scale[1] = (bds[3]-bds[2])/double(dim[1]-1);
      scale[2] = (bds[5]-bds[4])/double(dim[2]-1);

      //todo: instance by vol->GetMatrix()

      ospSet3f(OSPRayVolume, "gridOrigin", origin[0], origin[1], origin[2]);
      ospSet3f(OSPRayVolume, "gridSpacing", scale[0],scale[1],scale[2]);
      ospSetString(OSPRayVolume, "voxelType", voxelType.c_str());
      if (SharedData)
        {
        if (cacheEntry->VoxelData)
          ospRelease(cacheEntry->VoxelData);
        cacheEntry->VoxelData = ospNewData(sizeBytes, OSP_UCHAR, ScalarDataPointer, OSP_DATA_SHARED_BUFFER);
        ospSetData(OSPRayVolume, "voxelData", cacheEntry->VoxelData);
        }
      else
        {
        osp::vec3i ll, uu;
        ll.x = 0, ll.y = 0, ll.z = 0;
        uu.x = dim[0], uu.y = dim[1], uu.z = dim[2];
        ospSetRegion(OSPRayVolume, ScalarDataPointer, ll, uu);
        }

      cacheEntry->Volume = OSPRayVolume;
      cacheEntry->BuildTime.Modified();
      Cache[vol][timestep] = cacheEntry;
    }
    this->RenderTime = volNode->GetMTime();

    //
    // test for modifications to volume properties
    //
    vtkVolumeProperty* volProperty = vol->GetProperty();
    if (volProperty->GetMTime() > this->PropertyTime)
    {
      vtkColorTransferFunction* colorTF = volProperty->GetRGBTransferFunction(0);
      vtkPiecewiseFunction *scalarTF = volProperty->GetScalarOpacity(0);
      int numNodes = colorTF->GetSize();
      double* tfData = colorTF->GetDataPointer();

      this->TFVals.resize(this->NumColors*3);
      this->TFOVals.resize(this->NumColors);
      scalarTF->GetTable(data->GetScalarRange()[0],
                         data->GetScalarRange()[1],
                         this->NumColors,
                         &TFOVals[0]);
      colorTF->GetTable(data->GetScalarRange()[0],
                        data->GetScalarRange()[1],
                        this->NumColors,
                        &this->TFVals[0]);

      OSPData colorData = ospNewData(this->NumColors,
                                     OSP_FLOAT3,
                                     &this->TFVals[0]);
      ospSetData(this->TransferFunction, "colors", colorData);

      OSPData tfAlphaData = ospNewData(NumColors, OSP_FLOAT, &TFOVals[0]);
      ospSetData(this->TransferFunction, "opacities", tfAlphaData);

      ospCommit(this->TransferFunction);
      PropertyTime.Modified();
      ospRelease(colorData);
      ospRelease(tfAlphaData);
    }

    // test for modifications to input
    if (mapper->GetDataSetInput()->GetMTime() > this->BuildTime)
    {
      ospSet2f(this->TransferFunction, "valueRange",
               data->GetScalarRange()[0], data->GetScalarRange()[1]);

      //! Commit the transfer function only after the initial colors and alphas have been set (workaround for Qt signalling issue).
      ospCommit(this->TransferFunction);
    }
    ospSetObject((OSPObject)OSPRayVolume, "transferFunction", this->TransferFunction);
    this->BuildTime.Modified();

    if (this->SamplingRate == 0.0f)  // 0 means automatic sampling rate
      {
      //automatically determine sampling rate
      int maxBound = std::max(dim[0],dim[1]);
      maxBound = std::max(maxBound,dim[2]);
      float minSamplingRate = 0.125f;  //lower for minimum adaptive sampling step
      if (maxBound < 1000)
        {
        float s = 1000.0f - maxBound;
        s = (s/1000.0f*(1.0f-minSamplingRate) + minSamplingRate);
        ospSet1f(OSPRayVolume, "samplingRate", s);
        }
      else
        {
        ospSet1f(OSPRayVolume, "samplingRate", minSamplingRate);
        }
      }
    else
      {
      ospSet1f(OSPRayVolume, "samplingRate", this->SamplingRate);
      }
    ospSet1i(OSPRayVolume, "gradientShadingEnabled", volProperty->GetShade());
    ospCommit(OSPRayVolume);
    ospAddVolume(OSPRayModel,(OSPVolume)OSPRayVolume);
    ospCommit(OSPRayModel);
  }
}
