/*=========================================================================

  Program:   Visualization Toolkit
  Module:    vtkOSPRayRendererNode.h

  Copyright (c) Ken Martin, Will Schroeder, Bill Lorensen
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/Copyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
// .NAME vtkOSPRayRendererNode - links vtkRenderers to OSPRay
// .SECTION Description
// Translates vtkRenderer state into OSPRay rendering calls

#ifndef vtkOSPRayRendererNode_h
#define vtkOSPRayRendererNode_h

#include "vtkRenderingOSPRayModule.h" // For export macro
#include "vtkRendererNode.h"
#include <vector> // for ivars
#include <string>

class vtkRenderer;
class vtkInformationIntegerKey;
class vtkInformationStringKey;
// ospray forward decs so that someone does not need to include ospray.h
namespace osp {
struct Model;
struct Renderer;
struct Light;
struct Texture2D;
struct FrameBuffer;
}
typedef osp::Model *OSPModel;
typedef osp::Renderer *OSPRenderer;
typedef osp::Light *OSPLight;
typedef osp::FrameBuffer *OSPFrameBuffer;
typedef osp::Texture2D* OSPTexture2D;
typedef osp::FrameBuffer* OSPFrameBuffer;

class VTKRENDERINGOSPRAY_EXPORT vtkOSPRayRendererNode :
  public vtkRendererNode
{
public:
  static vtkOSPRayRendererNode* New();
  vtkTypeMacro(vtkOSPRayRendererNode, vtkRendererNode);
  void PrintSelf(ostream& os, vtkIndent indent);

  //Description:
  //Builds myself.
  virtual void Build(bool prepass);

  //Description:
  //Traverse graph in ospray's prefered order and render
  virtual void Render(bool prepass);

  //Description:
  //Put my results into the correct place in the provided pixel buffer.
  virtual void WriteLayer(unsigned char *buffer, float *zbuffer,
                          int buffx, int buffy, int layer);

  //state beyond rendering core...

  //Description:
  //When present on renderer, controls the number of primary rays
  //shot per pixel
  //default is 1
  static vtkInformationIntegerKey* SAMPLES_PER_PIXEL();

  //Description:
  //Convenience method to set/get SAMPLES_PER_PIXEL on a vtkRenderer.
  static void SetSamplesPerPixel(int, vtkRenderer *renderer);
  static int GetSamplesPerPixel(vtkRenderer *renderer);

  //Description:
  //When present on renderer, controls the number of ambient occlusion
  //samples shot per hit.
  //default is 4
  static vtkInformationIntegerKey* AMBIENT_SAMPLES();
  //Description:
  //Convenience method to set/get AMBIENT_SAMPLES on a vtkRenderer.
  static void SetAmbientSamples(int, vtkRenderer *renderer);
  static int GetAmbientSamples(vtkRenderer *renderer);

  //Description:
  //used to make the renderer add ospray's content onto GL rendered
  //content on the window
  static vtkInformationIntegerKey* COMPOSITE_ON_GL();
  //Description:
  //Convenience method to set/get COMPOSITE_ON_GL on a vtkRenderer.
  static void SetCompositeOnGL(int, vtkRenderer *renderer);
  static int GetCompositeOnGL(vtkRenderer *renderer);

  //Description:
  //using pathtracing or not
  static vtkInformationIntegerKey* PATHTRACING();

  //Description:
  //Get/Set wether to use path tracing for the renderer
  static void SetPathTracing(bool, vtkRenderer *renderer);
  static bool GetPathTracing(vtkRenderer *renderer);

  //Description:
  //When present on renderer, controls the number of maximum bounces of the light.
  //default is 5
  static vtkInformationIntegerKey * MAX_DEPTH();
  static void SetMaxDepth (int val, vtkRenderer *renderer);
  static int GetMaxDepth (vtkRenderer *renderer);

  //Description:
  //Convenience method to set/get SHOW_ENV_BACKGROUND on a vtkRenderer.
  static vtkInformationIntegerKey * SHOW_ENV_BACKGROUND();

  //Description:
  // Get/Set whether to show the environment map background
  static void SetShowEnvBackground (bool val, vtkRenderer *renderer);
  static bool GetShowEnvBackground (vtkRenderer *renderer);

  //Description:
  //Convenience method to set/get ENV_IMAGE_FILE on a vtkRenderer.
  static vtkInformationStringKey * ENV_IMAGE_FILE();
  //Description:
  // Get/Set the environment map light to use
  static void SetEnvImageFile (std::string val, vtkRenderer *renderer);
  static const char * GetEnvImageFile (vtkRenderer *renderer);

  // Description:
  // Methods for other nodes to access
  OSPModel GetOModel() { return this->OModel; }
  OSPModel SetOModel(OSPModel model) { this->OModel = model; }
  OSPRenderer GetORenderer() { return this->ORenderer; }
  void AddLight(OSPLight light) {
    this->Lights.push_back(light); }

  // Description:
  // Get the last rendered ColorBuffer
  virtual unsigned char *GetBuffer() {
    return this->Buffer; }

  // Description:
  // Get the last rendered ZBuffer
  virtual float *GetZBuffer() {
    return this->ZBuffer; }

  // Description:
  // Compute and fill the ZBuffer
  void SetComputeZBuffer(bool st) {
    ComputeDepth = st; }

  // if you want to traverse your children in a specific order
  // or way override this method
  virtual void Traverse(int operation);

protected:
  vtkOSPRayRendererNode();
  ~vtkOSPRayRendererNode();

  void UpdateOSPRayRenderer();

  //internal structures
  unsigned char *Buffer;
  float *ZBuffer;

  OSPModel OModel;
  OSPRenderer ORenderer;
  OSPFrameBuffer OFrameBuffer;
  int ImageX, ImageY;
  std::vector<OSPLight> Lights;
  std::string RendererStr;
  int NumActors;
  bool ComputeDepth;
  bool Accumulate;
  bool CompositeOnGL;
  unsigned long CameraTime;
  bool SceneDirty;
  unsigned long LastMTime;

private:
  vtkOSPRayRendererNode(const vtkOSPRayRendererNode&) VTK_DELETE_FUNCTION;
  void operator=(const vtkOSPRayRendererNode&) VTK_DELETE_FUNCTION;
};

namespace ospray {

  struct Texture2D {// : public RefCount {
    Texture2D();

    int channels; //Number of color channels per pixel
    int depth;    //Bytes per color channel
    bool prefereLinear; //A linear texel format is preferred over sRGB
    int width;    //Pixels per row
    int height;   //Pixels per column
    void *data;   //Pointer to binary texture data
  };

  Texture2D *loadTexture(const std::string &fileName, const bool prefereLinear = false);

}

#endif
